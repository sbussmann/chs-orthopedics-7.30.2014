##############################################################################
# Project: CHS - Joint Replacement
# Date: 7/30/14
# Author: Sam Bussmann
# Description: Bin up the data and create DS for modeling
# Notes: 
##############################################################################

library(gtools)


####### Get Vars Binned up ##########

cm<-data.frame(pt_flag=pm$pt_flag)
### Make sure to remove current formats from working directory if you have changed something below!

## gender, income, and age but may not want to have these in the model due to missings

viz("Gender")
#add("Gender")

viz("Income")
#add("Income")

viz("Age")
#add("Age")

table(pm$HomeOwner,pm$OwnRent)
out<-binmin(varname1="OwnRent",df=pm, minN=25000)
viz("OwnRent",out)
add("OwnRent",out,bin=T)

# viz("HomeOwner")
# add("HomeOwner")

# viz("RevolvingAccount")
# add("RevolvingAccount")

viz("spouseindicator")
out<-binmin(varname1="spouseindicator", minN=25000, df=pm)
viz("spouseindicator",out)
add("spouseindicator",out,bin=T)

# Varname<-"Denomination"
# out<-pm$Denomination
# out[(out %in% c("Buddhist", "Hindu", "Islamic", "Shinto", "Sikh","Jewish","Mormon","Unknown"))]<-"Other"
# out[(out %in% c("Catholic", "Eastern Orthodox", "Greek Orthodox" ))]<-"Catholic/Orthodox"
# out[(out %in% c("Lutheran"))]<-"Protestant"
# out<-data.frame(Denomination=out,pt_flag=pm$pt_flag)
# viz(Varname,out)
# add(Varname,out,bin=T)

Varname<-"HouseholdMemberCount"
out<-pm[,Varname]
out[out>3]<-"4+"
out<-data.frame(HouseholdMemberCount=out,pt_flag=pm$pt_flag)
viz(Varname,out)
add(Varname,out,bin=T)

table(pm$preschild_hosp,pm$NumberOfChildren,useNA="ifany")

viz("preschild_hosp")
add("preschild_hosp")

Varname<-"NumberOfChildren"
out<-binmin(varname1=Varname, minN=30000, df=pm)
viz(Varname,out)
# add(Varname,out,bin=T)

viz("creditcard_travel_and_entertainment")
viz("creditcard_bank")
viz("creditcard_premium_bank")
viz("creditcard_retail")
viz("creditcard_oil")
viz("creditcard_specialy_retail")
viz("creditcard_upscale_retail")
viz("creditcard_finance")
viz("creditcard_holder")

#add("creditcard_travel_and_entertainment")
add("creditcard_bank")
#add("creditcard_premium_bank")
add("creditcard_retail")
#add("creditcard_oil")
#add("creditcard_specialy_retail")
#add("creditcard_upscale_retail")
#add("creditcard_finance")
add("creditcard_holder")

Varname<-"mortgagepresent"
viz(Varname)
add(Varname)

# Varname<-"countryoforigin"
# viz(Varname)
# out<-pm[,Varname]
# out[(out %in% c("00"))]<-"Unknown"
# out[(out %in% c("PR"))]<-"Puerto Rico"
# out[(out %in% c("MX"))]<-"Mexico"
# out[(out %in% c("CO","BR","ES","PE","CU","DO","EC","GT"))]<-"Other Central/South America/Carribean"
# out<-data.frame(countryoforigin=out,pt_flag=pm$pt_flag)
# viz(Varname,out)
# add(Varname,out,bin=T)

Varname<-"vendorethnicgroup"
viz(Varname)
out<-binmin(varname1=Varname, minN=25000,df=pm)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"vendorreligion"
out<-binmin(varname1=Varname, minN=25000,df=pm)
viz(Varname,out)
add(Varname,out,bin=T)


### This really needs to be conditional
Varname<-"lengthofresidence"
out<-binmin(varname1=Varname, minN=40000,df=pm)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"homeage"
hist(pm[,Varname])
out<-ifelse(pm[,Varname]>55,"55+",
                          ifelse(pm[,Varname]>40,"41-55",
                                 ifelse(pm[,Varname]>25,"26-40",
                                        ifelse(pm[,Varname]>0,"1-25","0"))))
out[is.na(out)]<-"Unknown"
out<-data.frame(homeage=out,pt_flag=pm$pt_flag)
viz(Varname,out)
add(Varname,out,cdl=T)

Varname<-"locationtype"
etab(Varname)
out<-pm[,Varname]
out[(out %in% c("N","R","T"))]<-"Other"
out<-data.frame(locationtype=out,pt_flag=pm$pt_flag)
viz(Varname,out)
# add(Varname,out,bin=T)

Varname<-"phonenumbertype"
viz(Varname)
out<-pm[,Varname]
out[(out %in% c("V","O","D","F","M","T"))]<-"Voice"
out<-data.frame(phonenumbertype=out,pt_flag=pm$pt_flag)
viz(Varname,out)
add(Varname,out,bin=T)

### Bin and add all the age modeled categories

#for (i in which(names(pm)=="age0_3"):which(names(pm)=="markettargetage65plus")){
for (i in c(66:69,70,73:75,77:78,82:85,88:89,91:93)){  
  out<-binmin(varname1=names(pm)[i], minN=25000, df=pm)
  viz(names(pm)[i],out)
  if (length(levels(as.factor(out[,1])))>1) add(names(pm)[i],out,bin=T)
}

Varname<-"numberofadults"
etab(Varname)
out<-pm[,Varname]
out[out>3]<-"4+"
out<-data.frame(numberofadults=out,pt_flag=pm$pt_flag)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"heavy_internet_user"
out<-binmin(varname1=Varname, minN=35000,df=pm)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"sesi"
etab(Varname)
out<-binmin(varname1=Varname, minN=35000,df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,cdl=T)

Varname<-"wealthfinder"
etab(Varname)
out<-binmin(varname1=Varname,  minN=35000,df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"dmhightechhousehold"
etab(Varname)
viz(Varname)
add(Varname)

Varname<-"loantovalue"
etab(Varname)
out<-binmin(varname1=Varname,minN=35000,df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"findr"
etab(Varname)
viz(Varname)
out<-binmin(varname1=Varname,minN=35000,df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

#### Census Vars
### Handle these by leaving them as numeric, but standardizing 

for (i in c(which(names(pm)=="phhchild"):which(names(pm)=="mdoicost"),
            which(names(pm)=="pownrocc"):which(names(pm)=="phhspnsh"))){
  if (sum(is.na(pm[,names(pm)[i]]))==0) add(names(pm)[i],std=T)
}

Varname<-"homeagesource"
etab(Varname)
viz(Varname)
add(Varname)

Varname<-"homeequityestimate"
etab(Varname)
out<-binmin(varname1=Varname, minN=35000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"garagetype"
etab(Varname)
viz(Varname)
# add(Varname)

Varname<-"homevalue"
etab(Varname)
out<-binmin(varname1=Varname, minN=25000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"homevaluesource"
etab(Varname)
viz(Varname)
add(Varname)
#out<-binmin(varname1=Varname, minN=25000, df=pm)
#etab(Varname,out)
#viz(Varname,out)
#add(Varname,out,bin=T)

# Varname<-"homesize"
# hist(pm[,Varname],breaks=500,xlim=c(700,4000),ylim=c(0,2000))
# out<-ifelse(pm[,Varname]>2250,"2251+",
#             ifelse(pm[,Varname]>1750,"1751-2250",
#                    ifelse(pm[,Varname]>1300,"1301-1750",
#                           ifelse(pm[,Varname]>1000,"1001-1300",
#                                  ifelse(pm[,Varname]>0,"1-1000","Unknown")))))
# out<-data.frame(homesize=out,pt_flag=pm$pt_flag)
# etab(Varname,out)
# viz(Varname,out)
# add(Varname,out,cdl=T)

Varname<-"homesize"
hist(pm[,Varname],breaks=500,xlim=c(200,6000),ylim=c(0,5000))
out<-ifelse(pm[,Varname]>2000,"2000+",
            ifelse(pm[,Varname]>1500,"1501-2000",
                   ifelse(pm[,Varname]>1250,"1251-1500",
                          ifelse(pm[,Varname]>0,"1-1250","Unknown"))))
out<-data.frame(homesize=out,pt_flag=pm$pt_flag)
etab(Varname,out)
viz(Varname,out)
#add(Varname,out,cdl=T)

Varname<-"lotsize"
hist(pm[,Varname],breaks=1000,xlim=c(10,5000),ylim=c(0,50000))
out<-ifelse(pm[,Varname]>300,"300+",
                   ifelse(pm[,Varname]>0,"1-300","Unknown"))
out<-data.frame(lotsize=out,pt_flag=pm$pt_flag)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,cdl=T)

Varname<-"femaleoccupation"
viz(Varname)
etab(Varname)
out<-binmin(varname1=Varname, minN=30000, df=pm)
etab(Varname,out)
viz(Varname,out)
#add(Varname,out,bin=T)

Varname<-"maleoccupation"
viz(Varname)
out<-binmin(varname1=Varname, minN=25000, df=pm)
etab(Varname,out)
viz(Varname,out)
#add(Varname,out,bin=T)

# for (i in 166:225){
#   t<-table(pm[names(pm)[i]],useNA="ifany")
#   print(i)
#   print(names(pm)[i])
#   print(t)
# }

for (i in 166:225){
  if (sum(pm[,names(pm)[i]],na.rm=T)>35000) add(names(pm)[i])
}  


Varname<-"mortgageintrestratesource"
etab(Varname)
viz(Varname)
#add(Varname)
out<-binmin(varname1=Varname, minN=10000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"mortgageintrestrate"
out<-data.frame(mortgageintrestrate=
                  ifelse(is.na(pm[,Varname])," Missing",
                         ifelse(pm[,Varname]<430,"<4.3",
                                ifelse(pm[,Varname]<500,"[4.3-5)",   
                                       ifelse(pm[,Varname]<600,"[5-6)",
                                              ifelse(pm[,Varname]<700,"[6-7)","[7+)")))))
                ,pt_flag=pm$pt_flag)
etab(Varname,out)
viz(Varname,df=out)
add(Varname,out,cdl=T)


Varname<-"buyer_behavior_cluster_2004"
etab(Varname)
out<-binmin(varname1=Varname, minN=40000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,cdl=T)

table(pm$heavy_internet_user,pm$internetusage)

Varname<-"internetusage"
etab(Varname)
out<-binmin(varname1=Varname, minN=35000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"potentialinvestorconsumer"
etab(Varname)
out<-binmin(varname1=Varname, minN=45000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"revolverminimumpaymentmodel"
etab(Varname)
out<-binmin(varname1=Varname, minN=35000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"subfamilyindicator"
etab(Varname)
viz(Varname)
#add(Varname)

Varname<-"activeconsumer"
etab(Varname)
out<-binmin(varname1=Varname, minN=35000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

### lhi stuff
 
for (i in 245:351){
  out<-binmin(varname1=names(pm)[i], minN=35000, df=pm)
  viz(names(pm)[i],out)
  if (length(levels(as.factor(out[,1])))>1) add(names(pm)[i],out,bin=T)
}

Varname<-"csi_raw_score"
etab(Varname)
out<-binmin(varname1=Varname, minN=35000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,cdl=T)

#### More Census Stuff

for (i in 354:362){
  if (sum(is.na(pm[,names(pm)[i]]))==0) add(names(pm)[i],std=T)
}

Varname<-"mortgagefinancetype"
etab(Varname)
out<-binmin(varname1=Varname, minN=30000, df=pm)
etab(Varname,out)
viz(Varname,out)
add(Varname,out,bin=T)

Varname<-"homesaledatesourcecode"
etab(Varname)
viz(Varname)
add(Varname)

Varname<-"vacation_expense_category"
etab(Varname)
out<-binmin(varname1=Varname, minN=30000, df=pm)
etab(Varname,out)
viz(Varname,out)
#add(Varname,out,bin=T)

### tw stuff

for (i in 370:394){
  out<-binmin(varname1=names(pm)[i], minN=32500, df=pm)
 viz(names(pm)[i],out)
  if (length(levels(as.factor(out[,1])))>1) add(names(pm)[i],out,bin=T)
}

### Distance from nearest hospital

sum(is.na(pm$Longitude))
#persloc<-cbind(pm$Longitude,pm$Latitude)

library(RODBC)
sql<-odbcConnect("IrmSqls")
hosploc<-as.data.frame(sqlQuery(sql,
                                paste0("SELECT distinct 
                                name as facility,
                                Longitude,
                                Latitude
                                FROM ", dbname, ".dbo.ClientOrganization
                                WHERE ParentOrganizationID is not null
                                "),errors=T,stringsAsFactors = F))



### It looks like the Lat Longs got truncated or rounded
## The actual lat longs from google maps are below
options(digits=10)

hosploc<-hosploc[hosploc$facility %in% c("Kishwaukee Community Hospital", "Valley West Community Hospital"),]
#Kish
hosploc[1,2:3]<-c(-88.723530,41.962919)
#Valley West
hosploc[2,2:3]<-c(-88.622232,41.656729)

save(hosploc,file="hosploc.RData")

library(fields)
hosploc1<-cbind(hosploc$Longitude,hosploc$Latitude)

### Get the lat longs from the NationalMarket Database

### Get list of zips in service area
sql<-odbcConnect("IrmSqls")
zips<-sqlQuery(sql,paste0("SELECT distinct Zipcode 
               from ", dbname, ".dbo.starkarea"),errors=T,stringsAsFactors = F,as.is=T)

zips1<-NULL
for (i in 1:length(zips[[1]])){
if (i!=length(zips[[1]])) zips1<-paste0(zips1,"\'",zips[i,1],"\',") else
  zips1<-paste0(zips1,"\'",zips[i,1],"\'")
}

sql1<-odbcConnect("TncmSql1")
system.time(
addr<-as.data.frame(sqlQuery(sql1,
                             paste0("SELECT 
                             familyid,
                             longitude,
                             latitude
                             FROM [NationalMarket].[dbo].[NationalHousehold]
WHERE  (zipcode IN (", zips1 ,"))")
                             ,errors=T,stringsAsFactors = F))
)

### Merge familyid to pm$CommunityPersonID

sql<-odbcConnect("IrmSqls")
addrtocp<-as.data.frame(sqlQuery(sql,
                                 paste0("SELECT 
                                 CommunityPersonID,
                                 familyid
                                 from ", dbname, ".dbo.CommunityPerson as cp
                                 left join ", dbname, ".dbo.CommunityHousehold CH on ch.CommunityHouseholdID = cp.CommunityHouseholdID"),
                                 errors=T, stringsAsFactors = F))                               


interm<-merge(addrtocp,addr,by=c("familyid"),all.x=T)
pll<-merge(pm[,c("CommunityPersonID","zipcode")],interm[,c("CommunityPersonID","latitude","longitude")],
           by=c("CommunityPersonID"),all.x=T)

save(pll,file="pll.RData")
#load("pll.RData")

### Calc min dist

dist_out<-rdist.earth(pll[,c("longitude","latitude")],hosploc1)
#dist_out<-rdist.earth(pm[,c("Longitude","Latitude")],hosploc1)
mindist<-pmin(dist_out[,1],dist_out[,2],na.rm=T)
#mindist<-dist_out
hist(mindist)

## Impute missing distances
sum(is.na(mindist))
#mindist<-ifelse((is.na(mindist)),median(mindist,na.rm=T),mindist)

out<-data.frame(MinDist=mindist)
hist(out$MinDist,breaks=20)
add("MinDist",dfo=out,std=T)

###Bin up min dist just to see what it looks like

Varname<-"MinDist"
out1<-ifelse(mindist<7,"0-7",
            ifelse(mindist<13,"7-13",
                   ifelse(mindist<18,"13-17","18+")))
out<-data.frame(MinDist=out1 ,pt_flag=pm$pt_flag)
etab(Varname,out)
viz(Varname,out)
#add(Varname,out,cdl=T,dfo=data.frame(MinDist=mindist))

### Save 

save(cm,file="cm.RData")
#load("cm.RData")
