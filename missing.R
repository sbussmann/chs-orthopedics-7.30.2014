#########################
# Date: 4/22/2014
# Examine Missingness
#########################

## Missings
M<-sapply(addr2,function(x) sum(is.na(x)))
table(M)



## Blanks
B<-sapply(pmp,function(x) if (is.character(x)) sum((x==" BLANK"),na.rm=T) else 0)
table(B)


sum(cmty_area$homevalue==" ")

table(cmty_wtgt$homevalue, useNA="ifany")
table(cmty_wtgt$lhi_science, useNA="ifany")
table(cmty_wtgt$Gender, useNA="ifany")

